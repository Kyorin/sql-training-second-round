# Exercice de SQL

- [Base de données](./mysqlsampledatabase.sql) issue de : https://www.mysqltutorial.org/mysql-sample-database.aspx
- Exercises inspirés des exercices de : https://www.csharp-console-examples.com/sql/sql-queries-examples-with-answers/

## Consignes

Le fichier SQL à importer est mal conçu. Nous allons donc utiliser une fonctionnalité de mysql permettant d'ignorer les dépendances des Foreign Keys.
Et allons importer ce fichier SQL en désactivant l'option "Activer la vérification des clés étrangères".

Cette fonctionnalité est à éviter en général car il fait perdre le concept de l'ACID. Le consistence dans la base de données n'est pas garantie, ce qui est contraire à une base de données relationelle.

Réaliser les requetes suivantes :

- **Exercice 1: Lister tous les employés**
- **Exercice 2: Lister le nom, prénom et titre de chaque employé**
- **Exercice 3: Lister les Réprésenteant Commerciaux ('Sales Rep') de la table employés**
- **Exercice 4 : Lister les différents statuts de la table 'orders', sans doublons**
- **Exercice 5: Lister les paiements réalisés au mois de Mai**
- **Exercice 6: Lister les paiements réalisés en mai ou en juin 2005**
- **Exercice 7: Lister le nom et prénom des clients(customers) en les combinant au sein d'une seule colonne nommée 'fullName'**
- **Exercice 8: Lister les produits commençant par les caractères “195”**
- **Exercice 9: Lister le nom des produits qui ont un prix de vente (buyPrice) entre 70 et 100 $.**
- **Exercice 10: Lister les clients (customers) ayant l'un des prénoms suivants : 'Alexander', 'Daniel', 'Elizabeth'**
- **Exercice 11: Lister les clients ordonnés par Nom de famille en ordre descendant**
- **Exercice 12: Certains clients ont le même prénom. Réalisez une requête qui retourne les prénoms en doublons**
- **Exercice 13: Lister les 10 derniers enregistrements (en fonction de la colonne paymentDate) de la table payments**
- **Exercice 14: Retourner le paiement le plus cher**

- **Exercice 15: Retourner la ville d'un bureau (office city) accompagné par le nombre d'employés associés à ce bureau (officeCode)**

- **Exercice 16: Lettre à la DSI. Vous avez a reçu un courrier postal d'un client mais les informations sont à moitié effacées. Vous cherchez à retrouver qui celà peut bien être**
  - En déchiffrant la lettre, vous arrivez uniquement à récupérer le deuxième et troisième caractère du code postal... '78'.
    Réalisez une requête SQL permettant de lister les clients correspondants à ce code postal.\*\*
- **Exercice 17: Lister le chiffre d'affaire (somme des paiements), regroupés par année**
- **Exercice 18: Lister le chiffre d'affaire (somme des paiements), regroupés par année et mois. ordonnés chronologiquement**
- **Exercice 19: Vous organisez une tombola. Faites une requête permettant de sélectionner un client aléatoirement**
- **Exercice 20: Les employés ont des responsables (reportsTo). Retournez les employés n'ayant pas de supérieur**
- **Exercice 21: Retourner les employés (employeeNumber, firstName, lastName) ayant le plus de responsabilités (le plus de reportsTo associés)**
- **Exercice 22: Une erreur logicielle a eu lien, incrémentez toutes les factures de 5$.**
- **Exercice 23: Avez-vous remarqué que la table des paiements n'est pas liée aux orders? Comparez le montant des commandes par rapport aux paiements , regroupés par client**
- **Exercice 24: Les produits se vendent à des prix variants dans le temps. Retournez pour chaque produit le montant max et le montant min de la table orderdetails, ainsi que son prix actuel dans la table products**
